

using System.Threading;
using System.Threading.Tasks;
using BizCover.Api.Cars.Models.Requests;
using BizCover.Api.Cars.Models.Responses;
using MediatR;
using BizCover.Repository.Cars;
using AutoMapper;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace BizCover.Api.Cars.CommandHandlers
{

  public class GetCarsRequestHandler : IRequestHandler<GetCarsRequest, GetCarsResponse>
  {
    private readonly ICarRepository _carRepository;
    private readonly IMapper _mapper;
    private readonly ILogger<GetCarsRequestHandler> _logger;

    public GetCarsRequestHandler(ICarRepository carRepository, IMapper mapper, ILogger<GetCarsRequestHandler> logger)
    {
      _carRepository = carRepository;
      _mapper = mapper;
      _logger = logger;
    }

    public async Task<GetCarsResponse> Handle(GetCarsRequest request, CancellationToken cancellationToken)
    {
      var dbCars = await _carRepository.GetAllCars();

      return new GetCarsResponse()
      {
        Cars = _mapper.Map<List<Car>, List<Models.Common.Car>>(dbCars)
      };
    }
  }
}