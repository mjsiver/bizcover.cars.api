using AutoMapper;
using BizCover.Api.Cars.Models.Common;
using RepoCar = BizCover.Repository.Cars.Car;

namespace BizCover.Api.Cars.Automapper
{

  public class BizCoverMapProfile : Profile
  {
    public BizCoverMapProfile()
    {
      CreateMap<RepoCar, Car>().ReverseMap();
    }
  }
}