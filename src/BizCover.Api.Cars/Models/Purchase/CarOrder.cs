
using System.Collections.Generic;
using BizCover.Api.Cars.Models.Common;

namespace BizCover.Api.Cars.Models.Purchase
{
  public class CarOrder
  {
    public CarOrder()
    {
      Discounts = new List<Discount>();
    }
    
    public Car Car { get; set; }
    public List<Discount> Discounts { get; set; }
    public decimal Amount { get; set; }

    public override string ToString()
    {
      return Car.ToString();
    }
  }
}