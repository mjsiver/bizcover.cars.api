
using System.Collections.Generic;
using BizCover.Api.Cars.Models.Common;
using BizCover.Api.Cars.Models.Purchase;

namespace BizCover.Api.Cars.Models.Responses
{

  public class PurchaseCarsResponse
  {
    public List<CarOrder> CarOrders { get; set; }
    public int Count { get; set; }
    public decimal Total { get; set; }
    public decimal Discount { get; set; }
  }
}