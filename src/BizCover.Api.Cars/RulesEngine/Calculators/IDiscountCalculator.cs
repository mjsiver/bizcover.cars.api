

using System.Collections.Generic;
using BizCover.Api.Cars.Models.Purchase;

namespace BizCover.Api.Cars.RulesEngine.Calculators
{
  public interface IDiscountCalculator
  {
      void Calculate(List<CarOrder> carOrders);
  }
}