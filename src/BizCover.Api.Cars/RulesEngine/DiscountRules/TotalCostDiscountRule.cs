

using System.Collections.Generic;
using System.Linq;
using BizCover.Api.Cars.Extensions;
using BizCover.Api.Cars.Models.Purchase;

namespace BizCover.Api.Cars.RulesEngine.DiscountRules
{

  ///<summary>
  /// If total cost exceeds $100,000 apply 5% discount
  ///</summary>
  public class TotalCostDiscountRule : IDiscountRule
  {
    const decimal DiscountPercentage = 5M;

    public void Evaluate(List<CarOrder> carOrders)
    {
        var totalCost = carOrders.Sum(x => x.Car.Price);
        
        if(totalCost > 100000M)
        {
          foreach(var carOrder in carOrders)
          {
            carOrder.Discounts.Add(new Models.Purchase.Discount() {
              Type = this.GetType().Name,
              Percentage = DiscountPercentage.DisplayPercentage(),
              Less = carOrder.Car.Price.ApplyDiscount(DiscountPercentage)
            });
          }
        }
    }
  }
}