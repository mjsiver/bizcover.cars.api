
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BizCover.Api.Cars.CommandHandlers;
using BizCover.Api.Cars.Extensions;
using BizCover.Api.Cars.RulesEngine.DiscountRules;
using BizCover.Api.Cars.Tests.Common;
using BizCover.Repository.Cars;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace BizCover.Api.Cars.Tests.RulesEngine.DiscountRules
{
    public class TotalQuantityDiscountRuleTests
    {
        private readonly TotalQuantityDiscountRule _rule;
        public TotalQuantityDiscountRuleTests()
        {
          _rule = new TotalQuantityDiscountRule();
        }

        [Fact]
        public void WhenTotalQuanitityOfCarsExeeds_2_Then_3_PercentDiscountMustBeAppliedOnAllCars()
        {
          var cars = new List<Car>() {
            new Car() {
              Price = 50000
            },
            new Car() {
              Price = 60000
            },
            new Car() {
              Price = 10000
            }
          };
          var carOrders = MockHelper.GetCarOrders(cars);
          _rule.Evaluate(carOrders);

          // 10% discount applied to all cars
          var totalQuantityDiscountsApplied = MockHelper.DiscountsApplied<TotalQuantityDiscountRule>(carOrders, 3M);
          Assert.Equal(carOrders.Count, totalQuantityDiscountsApplied.Count());
        }

        [Fact]
        public void WhenTotalQuanitityOfCarsIsLessThan_2_Then_NoTotalQuantityDiscountBeAppliedOnOnAnyCar()
        {
          var cars = new List<Car>() {
            new Car() {
              Price = 50000
            },
            new Car() {
              Price = 50000
            }
          };
          var carOrders = MockHelper.GetCarOrders(cars);
          _rule.Evaluate(carOrders);

          // discount not applied on any car
          var totalQuantityDiscountsApplied = MockHelper.DiscountsApplied<TotalQuantityDiscountRule>(carOrders, 3M);
          Assert.Equal(0, totalQuantityDiscountsApplied.Count());

        }
    }
}
