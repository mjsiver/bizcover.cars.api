
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BizCover.Api.Cars.CommandHandlers;
using BizCover.Api.Cars.RulesEngine.DiscountRules;
using BizCover.Api.Cars.Tests.Common;
using BizCover.Repository.Cars;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace BizCover.Api.Cars.Tests.RulesEngine.DiscountRules
{
    public class TotalCostDiscountRuleTests
    {
        private readonly TotalCostDiscountRule _rule;
        public TotalCostDiscountRuleTests()
        {
          _rule = new TotalCostDiscountRule();
        }

        [Fact]
        public void WhenTotalCostOfCarsExeeds_100000_Then_5_PercentDiscountMustBeAppliedOnAllCars()
        {
          var cars = new List<Car>() {
            new Car() {
              Price = 50000
            },
            new Car() {
              Price = 60000
            }
          };
          var carOrders = MockHelper.GetCarOrders(cars);
          _rule.Evaluate(carOrders);

          // 5% discount applied to all cars
          var totalCostDiscountsApplied = carOrders.SelectMany(c => c.Discounts).Where(d => string.Equals(d.Type, nameof(TotalCostDiscountRule)));
          Assert.Equal(carOrders.Count, totalCostDiscountsApplied.Count());

        }

        [Fact]
        public void WhenTotalCostOfCarsIsLessThanEqual_100000_Then_TotalCostDiscountIsNotAppliedOnAnyCar()
        {
          var cars = new List<Car>() {
            new Car() {
              Price = 50000
            },
            new Car() {
              Price = 50000
            }
          };
          var carOrders = MockHelper.GetCarOrders(cars);
          _rule.Evaluate(carOrders);

          // discount not applied on any car
          var totalCostDiscountsApplied = MockHelper.DiscountsApplied<TotalCostDiscountRule>(carOrders, 5M);
          
          Assert.Equal(0, totalCostDiscountsApplied.Count());

        }

        
    }
}
