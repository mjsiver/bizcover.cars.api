
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BizCover.Api.Cars.CommandHandlers;
using BizCover.Api.Cars.Extensions;
using BizCover.Api.Cars.RulesEngine.DiscountRules;
using BizCover.Api.Cars.Tests.Common;
using BizCover.Repository.Cars;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace BizCover.Api.Cars.Tests.RulesEngine.DiscountRules
{
    public class YearOfManufactureDiscountRuleTests
    {
        private readonly YearOfManufactureDiscountRule _rule;
        public YearOfManufactureDiscountRuleTests()
        {
          _rule = new YearOfManufactureDiscountRule();
        }

        [Fact]
        public void WhenYearOfManufactureIsLessThan_2000_Then_10_PercentDiscountMustBeAppliedOnlyOnThatCar()
        {
          var cars = new List<Car>() {
            new Car() {
              Year = 1900
            },
            new Car() {
              Year = 1990
            },
            new Car() {
              Year = 2000
            },
            new Car() {
              Year = 2020
            }
          };
          var carOrders = MockHelper.GetCarOrders(cars);
          _rule.Evaluate(carOrders);

          // 10% discount applied to cars
          var totalQuantityDiscountsApplied = MockHelper.DiscountsApplied<YearOfManufactureDiscountRule>(carOrders, 10M);
          var carsPriorToYear2000 = carOrders.Where(x => x.Car.Year < 2000);

          Assert.Equal(carsPriorToYear2000.Count(), totalQuantityDiscountsApplied.Count());
        }
    }
}
