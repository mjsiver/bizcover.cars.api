
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BizCover.Api.Cars.CommandHandlers;
using BizCover.Api.Cars.RulesEngine.Calculators;
using BizCover.Api.Cars.Tests.Common;
using BizCover.Repository.Cars;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace BizCover.Api.Cars.Tests.CommandHandlers
{
    public class PurchaseCarsRequestHandlerTests
    {
        private readonly Mock<ICarRepository> _carRepositoryMock;
        private readonly Mock<IDiscountCalculator> _discountCalculator;
        private readonly Mock<ILogger<PurchaseCarsRequestHandler>> _loggerMock;
        private readonly PurchaseCarsRequestHandler _handler;
        public PurchaseCarsRequestHandlerTests()
        {
          _carRepositoryMock = MockHelper.MockCarRepository();
          _discountCalculator = MockHelper.MockDiscountCalculator();
          _loggerMock = MockHelper.MockLogger<PurchaseCarsRequestHandler>();
          _handler = new PurchaseCarsRequestHandler(_carRepositoryMock.Object, _discountCalculator.Object, MockHelper.GetAutomapperObject(), _loggerMock.Object);
        }

        [Fact]
        public async Task WhenRequestPurchaseCarOrders_ThenCarsMatchingIdsShouldBeReturnedSuccessfully()
        {
            var request = new Models.Requests.PurchaseCarsRequest() 
            {
                CarIds = new List<int>() {
                    1,2,3,4,5,6,7,8,9,10,1000,10000,100000
                }
            };

            var response = await _handler.Handle(request, CancellationToken.None);

            var matchingCarsInRepositoryCount = MockHelper.GetCars().Where(x => request.CarIds.Contains(x.Id)).Count();

            Assert.Equal(response.CarOrders.Count, matchingCarsInRepositoryCount);
        }
    }
}
